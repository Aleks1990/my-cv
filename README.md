<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Alex`s Personal Site</title>
</head>

<body>
  <table cellspacing="20">
    <tr>
      <td>
        <img src="images/Alex_pic.jpg" alt="картинка с моей мордой" height="200">
      </td>
      <td>
        <h1>Александра Пустовойтова</h1>
        <p>
          <em>Инженер и <strong>будущий путешественник в <a href="https://turclub-pik.ru/">Тур клуб Пик</a></strong></em>
        </p>
        <p>
          Я люблю животных и гулять по лесам. А также обожаю шоппинг, когда у меня достаточно на него денег.
        </p>
      </td>
    </tr>
  </table>

  <hr>
  <h3>Список учебных заведений, которые были окончены</h3>
  <ul>
    <li>Школа №32</li>
    <ul>
      <li>
        Класс с экономическим уклоном, 11А
      </li>
    </ul>
  </ul>
  <ul>
    <li>
      Закончила <a href="http://pnu.edu.ru/ru/">ТОГУ</a>
    </li>
    <ul>
      <li>
        Факультет Управление и информатика в технических системах
      </li>
      <li>
        Получила диплом с квалификацией - инженер
      </li>
    </ul>
  </ul>
  <ul>
    <li>
      С декабря 2013 года работаю в <a href="http://sms-dv.ru/">НВФ "СМС ДВ"</a> в должности инженера-проектировщика
    </li>
  </ul>
  <hr />
  <h3>Опыт работы</h3>
  <table cellspacing="10">
    <thead>
      <th>Дата</th>
      <th>Место</th>
    </thead>
    <tbody>
      <tr>
        <td>2012-2013</td>
        <td>Тестировщик в Ланит-Партнер</td>
      </tr>
      <tr>
        <td>2013 - по настоящее время</td>
        <td>Инженер-проектировщик АСУ ТП в НВФ СМС ДВ</td>
      </tr>
    </tbody>
  </table>
  <hr />
  <h3>Навыки</h3>
        <table cellspacing="10">
          <tbody>
            <tr>
              <td>Рисование электрических принципиальных схем</td>
              <td>💛💛💛💛</td>
            </tr>
            <tr>
              <td>Программирование</td>
              <td>💛</td>
            </tr>
            <tr>
              <td>Плавание</td>
              <td>💛💛</td>
            </tr>
          </tbody>
        </table>
  <hr />
  <a href="hobbies.html">Мои хобби</a>
  <a href="contacts.html">Мои контакты</a>


</body>

</html>
